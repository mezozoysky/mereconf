## mereconf

Simple dictionary-based configuration load/save/access library.

License: MIT.

### Installation

From source:

* ``git clone git://gitlab.com/mezozoysky/mereconf.git``
* ``cd mereconf``
* ``python3 setup.py install``

### Requirements

* Python 3

### Limitations

* Current version works with Json configurations only

### Getting Help
    
* Email me: Stanislav Demyanovich <mezozoysky@gmail.com>
* Post issues to Gitlab: <http://gitlab.com/mezozoysky/mereconf/issues>

