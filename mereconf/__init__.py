# -*- coding: utf-8 -*-

import os
from os import path as osp
import json
import collections


class ConfigException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class Config(object):

    _instance = None

    _path_sep = '.'


    @classmethod
    def set_instance(cls, the_instance):
        if the_instance is not None and not isinstance(the_instance, Config):
            raise TypeError('Instance must be of mereconf.Config type,'
                            ' not {}'.format(type(the_instance).__qualname__))
        cls._instance = the_instance


    @classmethod
    def instance(cls):
        return cls._instance


    @property
    def base_dir(self):
        return self._base_dir

    @base_dir.setter
    def base_dir(self, base_dir):
        if base_dir is not None:
            base_dir = osp.normpath(osp.expanduser(base_dir))
            if not osp.isabs(base_dir):
                base_dir = osp.abspath(base_dir)
            if not osp.exists(base_dir) or not osp.isdir(base_dir):
                    raise FileNotFoundError('Directory "{}" doesnt exist'
                                            .format(base_dir))
        self._base_dir = base_dir


    def __init__(self, initial_dict = {}, **kwargs):
        if not isinstance(initial_dict, dict):
            raise TypeError('Initial dict must be a dict, not {}'
                            .format(type(initial_dict).__qualname__))
        if 'path_sep' in kwargs:
            self._path_sep = kwargs['path_sep']

        base_dir = kwargs.get('base_dir')
        if base_dir is None:
            base_dir = os.getcwd()
        self.base_dir = base_dir

        self._root = initial_dict


    def set(self, path, value, **kwargs):
        path_sep = kwargs.get('path_sep')
        if path_sep is None:
            path_sep = self._path_sep

        path_keys = None
        if path:
            path_keys = path.split(path_sep)

        if not path_keys:
            if not isinstance(value, dict):
                raise TypeError('Cant set root config with the given value;'
                                ' expected: dict, given: {}'
                                .format(type(value).__qualname__))
            self._root = value
            return self._root

        subdict = self._root
        for index, path_key in enumerate(path_keys):
            if index == len(path_keys) - 1: #last path key
                subdict[path_key] = value
                return subdict[path_key]

            subdict_next = subdict.get(path_key)

            if subdict_next is None:
                raise KeyError('Subconfig "{}" (path: "{}") doesnt exist'
                                   .format(path_key, path))
            if not isinstance(subdict_next, dict):
                raise TypeError('Subconfig "{}" (path: "{}") is not a dict'
                                .format(path_key, path))

            subdict = subdict_next


    def get(self, path, **kwargs):
        path_sep = kwargs.get('path_sep')
        if path_sep is None:
            path_sep = self._path_sep
        default_val = kwargs.get('default')

        path_keys = None
        if path:
            path_keys = path.split(path_sep)

        if not path_keys:
            return self._root

        subdict = self._root
        for index, path_key in enumerate(path_keys):
            subdict_next = subdict.get(path_key)
            if index < len(path_keys) - 1:
                if subdict_next is None:
                    raise KeyError('Subconfig "{}" (path: "{}") doesnt exist'
                                   .format(path_key, path))
                if not isinstance(subdict_next, dict):
                    raise TypeError('Subconfig "{}" (path: "{}") is not a dict'
                                    .format(path_key, path))

            subdict = subdict_next

        return subdict


    def load(self, path, file_path, **kwargs):
        path_sep = kwargs.get('path_sep')
        if path_sep is None:
            path_sep = self._path_sep

        validate = kwargs.get('validate')
        if validate is not None and \
                not isinstance(validate, collections.Callable):
            raise TypeError('Argument "validate" must be a function-like,'
                            ' but given: {}'
                            .format(type(validate).__qualname__))

        skip_no_file = bool(kwargs.get('skip_no_file'))
        no_file = False
        file_path = osp.normpath(osp.expanduser(file_path))
        if not osp.isabs(file_path):
            file_path = osp.normpath(osp.join(self._base_dir, file_path))
        if not osp.exists(file_path) or not osp.isfile(file_path):
            if skip_no_file:
                no_file = True
            else:
                raise FileNotFoundError('Config file "{}" doesnt exist'
                                        .format(file_path))

        path_keys = None
        if path:
            path_keys = path.split(path_sep)

        if not path_keys:
            if no_file and skip_no_file:
                self._root = {}
            else:
                self._root = self._load_dict_from_file(file_path)
            if validate is not None:
                validate(self._root, cfg_file=file_path)
            return self._root

        subdict = self._root
        for index, path_key in enumerate(path_keys):
            if index == len(path_keys) - 1:
                if no_file and skip_no_file:
                    subdict[path_key] = {}
                else:
                    subdict[path_key] = self._load_dict_from_file(file_path)
                if validate is not None:
                    validate(subdict[path_key], cfg_file=file_path)
                return subdict[path_key]

            subdict_next = subdict.get(path_key)

            if subdict_next is None:
                raise KeyError('Subconfig "{}" (path: "{}") doesnt exist'
                                   .format(path_key, path))
            if not isinstance(subdict_next, dict):
                raise TypeError('Subconfig "{}" (path: "{}") is not a dict'
                                .format(path_key, path))

            subdict = subdict_next


    def save(self, path, file_path, **kwargs):
        path_sep = kwargs.get('path_sep')
        if path_sep is None:
            path_sep = self._path_sep

        file_path = osp.normpath(osp.expanduser(file_path))
        if not osp.isabs(file_path):
            file_path = osp.normpath(osp.join(self._base_dir, file_path))
        if osp.exists(file_path) and not osp.isfile(file_path):
            raise RuntimeError('Config file path "{}" exists but doesnt'
                               'point to a file'.format(file_path))

        path_keys = None
        if path:
            path_keys = path.split(path_sep)

        if not path_keys:
            self._save_dict_to_file(file_path, self._root)
            return

        subdict = self._root
        for index, path_key in enumerate(path_keys):
            subdict_next = subdict.get(path_key)
            if index < len(path_keys) - 1:
                if subdict_next is None:
                    raise KeyError('Subconfig "{}" (path: "{}") doesnt exist'
                                   .format(path_key, path))
                if not isinstance(subdict_next, dict):
                    raise TypeError('Subconfig "{}" (path: "{}") is not a dict'
                                    .format(path_key, path))

            subdict = subdict_next

        self._save_dict_to_file(file_path, subdict)


    def _load_dict_from_file(self, file_path):
        assert osp.isabs(file_path), \
            'Config file path must be absolute, but given: "{}"' \
            ''.format(file_path)
        if not osp.exists(file_path):
            raise FileNotFoundError('Configuration "{}" doesnt exist'
                                    .format(file_path))
        the_dict = None
        the_dict = json.load(open(file_path))

        return the_dict


    def _save_dict_to_file(self, file_path, the_dict):
        assert osp.isabs(file_path), \
            'Config file path must be absolute, but given: "{}"' \
            ''.format(file_path)
        if osp.exists(file_path):
            os.remove(file_path)
        with open(file_path, 'w') as file:
            json.dump(the_dict, file, indent=4, sort_keys=True)

