#!/usr/bin/env python3

from mereconf import Config, ConfigException


def validate_section2(section2, **kwargs):
    file_name = kwargs.get('cfg_file')
    if section2['s2_k2'] < 10:
        section2['s2_k2'] += 10

    donuts = section2.get('donuts')
    if donuts is None:
        section2['donuts'] = 'BIG FAT DONUTS'
    del donuts

    if not 'mandatory' in section2:
        raise ConfigException('No "mandatory" field found within config "{}"'
                              .format(file_name))


def load_section2():
    Config.instance().load('section2', 'section2.json',
                           validate=validate_section2)


if __name__ == '__main__':
    Config.set_instance(Config({ 'i1' : 1, 'i2' : 2, 'i8' : "EIGHT", },
                               base_dir='./sample_base_dir'))
    cfg = Config.instance()

    cfg.set('new_section', { 'some_key' : 'some_val', })
    cfg.load('section1', 'section1.json')
    load_section2()
    print('Config: {}'.format(cfg.get('')))
    cfg.save('', 'full_config.json')

