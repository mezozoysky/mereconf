#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from setuptools import setup, find_packages

top_dir = os.path.abspath(os.path.dirname(__file__))
package_name = 'mereconf'
package_desc = 'Simple and small dictionary-based configuration library'
package_version = None
with open(os.path.join(top_dir, package_name, '__version__.py')) as f:
    tmp = {}
    exec(f.read(), tmp)
    package_version = tmp['__version__']
    del tmp
package_requires = open("requirements.txt").read().split('\n')
readme_content = open("README.md").read()

setup(
    name=package_name,
    version=package_version,
    description=package_desc,
    long_description=readme_content,
    author='Stanislav Demyanovich',
    author_email='mezozoysky@gmail.com',
    license='MIT',
    keywords="configuration development",
    url='http://gitlab.com/mezozoysky/mereconf',
    packages=find_packages(exclude=('tests')),
    include_package_data=True,
    install_requires=package_requires,
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.4.2',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.6.4',
        'Programming Language :: Python :: 3.6.5',
        'Programming Language :: Python :: 3.6.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.7.4',
        'Programming Language :: Python :: 3.7.9',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.8.6',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.9.6',
    ],
)
